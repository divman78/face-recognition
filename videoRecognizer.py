from tkinter import *
import os
import cv2
import numpy as np
import pickle



def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


#Given an image below function returns rectangle for face detected alongwith gray scale image
def faceDetection(test_img):
    gray_img=cv2.cvtColor(test_img,cv2.COLOR_BGR2GRAY)#convert color image to grayscale
    face_haar_cascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')#Load haar classifier
    faces=face_haar_cascade.detectMultiScale(gray_img,scaleFactor=1.32,minNeighbors=5)#detectMultiScale returns rectangles

    return faces,gray_img

#This module captures images via webcam and performs face recognition
face_recognizer = cv2.face.LBPHFaceRecognizer_create()
face_recognizer.read('recognizer.yml')#Load saved training data
namestoFaceIDs = load_obj('namestoFaceIDs')
print(namestoFaceIDs)


cap=cv2.VideoCapture(0)

newfaceCount = 0
b = True

while True:
    ret,test_img=cap.read()# captures frame and returns boolean value and captured image
    faces_detected,gray_img=faceDetection(test_img)



    for (x,y,w,h) in faces_detected:
      cv2.rectangle(test_img,(x,y),(x+w,y+h),(255,0,0),thickness=1)

    cv2.imshow('face recognition ', test_img)
    for face in faces_detected:
        (x,y,w,h)=face
        roi_gray=gray_img[y:y+w, x:x+h]
        label,confidence=face_recognizer.predict(roi_gray)#predicting the label of given image
        print("confidence:",confidence)
        print("label:",label)
        name = namestoFaceIDs[label]
        print(name)
        if confidence < 100:#If confidence less than 30 then don't print predicted face text on screen
           print(name)
           cv2.putText(test_img,name+" "+str(int(confidence)),(x,y),cv2.FONT_HERSHEY_DUPLEX,1,(255,0,0),1)
        else:
        	newfaceCount = newfaceCount+1
    cv2.imshow('face recognition ',test_img)
    cv2.waitKey(10)

    if cv2.waitKey(10) == ord('q'):#wait until 'q' key is pressed
        break



cap.release()
cv2.destroyAllWindows




