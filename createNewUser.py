from tkinter import *
import os
import cv2
import numpy as np


#Given an image below function returns rectangle for face detected alongwith gray scale image
def faceDetection(test_img):
    gray_img=cv2.cvtColor(test_img,cv2.COLOR_BGR2GRAY)#convert color image to grayscale
    face_haar_cascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')#Load haar classifier
    faces=face_haar_cascade.detectMultiScale(gray_img,scaleFactor=1.32,minNeighbors=5)#detectMultiScale returns rectangles

    return faces,gray_img


def save_face():
	path = str(e1.get())
	print(type(path))
	print(path)
	try:  
		os.mkdir("./trainingImages/"+path)
	except OSError:  
	    print ("Already created")
	else:  
	    print ("Successfully created the directory")
	master.quit()
	test_img = cv2.resize(test_img, (640, 480))
	cv2.imwrite("./trainingImages/"+path+ "/"+str(count)+".jpg", test_img)
   #print("First Name: %s\nLast Name: %s" % (e1.get()))


def save_faces(test_img):
	# cv2.imshow("SDfs",test_img)
	# cv2.waitKey(0)
	path = str(e1.get())
	print(type(path))
	print(path)
	try:  
		os.mkdir("./trainingImages/"+path)
	except OSError:  
	    print ("Already created")
	else:  
	    print ("Successfully created the directory")
	test_img = cv2.resize(test_img, (640, 480))
	cv2.imwrite("./trainingImages/"+path+ "/"+str(count)+".jpg", test_img)
   	#print("First Name: %s\nLast Name: %s" % (e1.get()))

cap=cv2.VideoCapture(0)

newfaceCount = 0
b = True

while b:
    b = False
    ret,test_img=cap.read()# captures frame and returns boolean value and captured image
    faces_detected,gray_img=faceDetection(test_img)

    cv2.imshow('face recognition', test_img)
    if(len(faces_detected) == 1):
        (x,y,w,h)=faces_detected[0]
        roi_gray=gray_img[y:y+w, x:x+h]
        master = Tk()
        Label(master, text="Enter Your Name").grid(row=0)
        e1 = Entry(master)
        e1.grid(row=0, column=1)
        count = 11 #number of images
        Button(master, text='OK', command=save_face).grid(row=3, column=0, sticky=W, pady=4)
        mainloop( )
        count = count - 1
        while(count != 0):
        	ret,test_img=cap.read()
        	cv2.imshow('face recognition', test_img)
        	save_faces(test_img)
        	count = count - 1

    else:
        b = True

    cv2.imshow('face recognition', test_img)
    cv2.waitKey(10)

    if cv2.waitKey(10) == ord('q'):#wait until 'q' key is pressed
        break

print("DONE")

cap.release()
cv2.destroyAllWindows




